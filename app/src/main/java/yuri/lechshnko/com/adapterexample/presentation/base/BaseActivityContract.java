package yuri.lechshnko.com.adapterexample.presentation.base;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

public interface BaseActivityContract {
    <T>void setData(T data);

    <T>void transactionActivity(Class<?> activity,T object);

    void transactionFragmentDialog(DialogFragment fragment);

    void transactionActivity(Class<?> activity);

    void transitionFragment(Fragment fragment);

    void transitionFragmentDialog(DialogFragment fragment);
}
