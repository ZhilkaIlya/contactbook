package yuri.lechshnko.com.adapterexample.presentation.fragment.main;

import android.util.Log;

import java.util.List;

import yuri.lechshnko.com.adapterexample.data.model.Contact;
import yuri.lechshnko.com.adapterexample.presentation.base.BaseFragmentContract;
import yuri.lechshnko.com.adapterexample.presentation.base.BasePresenter;

public interface MainFragmentContract {
    interface View extends BaseFragmentContract {

        void initAdapter(List<Contact> list);

        void addItem(Contact contact);

        void deleteItem(int id);

        void message(String val);

        void setName(String string);

        void setNumber(String string);

        Contact eventAddContact();

        String getName();

        String getNumber();

    }

    interface Presenter extends BasePresenter<View>{

        void init();

        void eventUpdateItem(Contact item);

        void eventDeleteItem(int id,String name,String number);

        void eventAddItem();

    }
}
