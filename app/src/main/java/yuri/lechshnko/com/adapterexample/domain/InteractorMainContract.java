package yuri.lechshnko.com.adapterexample.domain;

import java.util.List;

import io.reactivex.Single;
import yuri.lechshnko.com.adapterexample.data.model.Contact;

public interface InteractorMainContract {
    Single<Long> create(String name,String number);

    Single<List<Contact>> read();

//    Single<Contact> update(Contact contact);
Single<Contact> update(Contact contact);

    Single<Long> delete(Contact contact);

//    Single<Long> delete(long id);
}
