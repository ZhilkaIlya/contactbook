package yuri.lechshnko.com.adapterexample.presentation;

import io.reactivex.Single;
import yuri.lechshnko.com.adapterexample.Constant;
import yuri.lechshnko.com.adapterexample.data.model.Contact;
import yuri.lechshnko.com.adapterexample.presentation.base.BaseActivityContract;
import yuri.lechshnko.com.adapterexample.presentation.base.BaseDialogFragment;
import yuri.lechshnko.com.adapterexample.presentation.fragment.dialog.DialogUpdateFragment;
import yuri.lechshnko.com.adapterexample.presentation.fragment.main.MainFragment;

public class Route implements IRoute{
    private static IRoute instance;
    private BaseActivityContract view;

    private Route() {
    }

    public static synchronized IRoute getInstance(){
        if (instance == null){
            instance = new Route();
        }
        return instance;
    }

    @Override
    public void startView(BaseActivityContract view) {
        if (view != null) {
            this.view = view;
        }
    }

    @Override
    public void stopView() {
        if (view != null) view = null;
    }

    @Override
    public <T> void transaction(String cmd, T contact) {
        if (view != null) {
            if (Constant.MAIN_F.equals(cmd)) {
                view.transitionFragment(MainFragment.newInstance());
            }
        }
    }

    public Single<Contact> startDialog(Contact contact){
        return stepDialog(DialogUpdateFragment.newInstance(contact));
    }


    private <T extends BaseDialogFragment> Single<Contact> stepDialog(T fragment) {
        view.transitionFragmentDialog(fragment);
        return Single.create(fragment::setEmitter);
    }

    @Override
    public <T> void setDataDialog(T data) {
        if (view!= null) view.setData(data);
    }
}
