package yuri.lechshnko.com.adapterexample.presentation.fragment.dialog;

import yuri.lechshnko.com.adapterexample.data.model.Contact;

public class DialogUpdatePresenter implements DialogUpdateContract.Presenter {
    private Contact contact;
    private DialogUpdateContract.View view;

    @Override
    public void init(Contact contact) {
        this.contact = contact;
    }

    @Override
    public void onClickOk() {
        contact.setName(view.getName());
        contact.setNumber(view.getNumber());
        view.nextStep(contact);
    }

    @Override
    public void onClickCancel() {
        view.stepError(new Throwable());
    }

    @Override
    public void startView(DialogUpdateContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        if (view != null) view = null;
        if (contact != null) contact = null;
    }
}
