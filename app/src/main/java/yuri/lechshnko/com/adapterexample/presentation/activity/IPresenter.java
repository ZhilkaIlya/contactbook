package yuri.lechshnko.com.adapterexample.presentation.activity;


import yuri.lechshnko.com.adapterexample.presentation.base.BaseActivityContract;
import yuri.lechshnko.com.adapterexample.presentation.base.BasePresenter;

public interface IPresenter {
     interface View extends BaseActivityContract {

    }

     interface Listener extends BasePresenter<View> {
        void init();
    }
}
