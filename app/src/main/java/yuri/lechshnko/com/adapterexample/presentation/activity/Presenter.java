package yuri.lechshnko.com.adapterexample.presentation.activity;


import yuri.lechshnko.com.adapterexample.Constant;
import yuri.lechshnko.com.adapterexample.domain.InteractorMainContract;
import yuri.lechshnko.com.adapterexample.presentation.Route;


public class Presenter implements IPresenter.Listener {
    private IPresenter.View view;
    private InteractorMainContract interactor;


    @Override
    public void startView(IPresenter.View view) {
         this.view = view;
    }

    @Override
    public void detachView() {
        if (view != null) view = null;
    }

    @Override
    public void init()  {
        Route.getInstance().transaction(Constant.MAIN_F,null);
    }


}
