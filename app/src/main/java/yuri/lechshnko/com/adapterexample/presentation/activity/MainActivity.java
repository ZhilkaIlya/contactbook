package yuri.lechshnko.com.adapterexample.presentation.activity;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import yuri.lechshnko.com.adapterexample.data.model.Contact;
import yuri.lechshnko.com.adapterexample.presentation.Route;
import yuri.lechshnko.com.adapterexample.presentation.adapter.AdapterExample;
import yuri.lechshnko.com.adapterexample.presentation.base.BaseActivity;
import yuri.lechshnko.com.adapterexample.presentation.base.BasePresenter;
import yuri.lechshnko.com.adapterexample.R;
import yuri.lechshnko.com.adapterexample.databinding.ActivityMainBinding;

public class MainActivity extends BaseActivity<ActivityMainBinding> implements IPresenter.View {
    private IPresenter.Listener presenter;
    private AdapterExample adapter;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {
        presenter = new Presenter();
        Route.getInstance().startView(this);
        getBinding().setEvent(presenter);
    }

    @Override
    protected void onStartView() {
        presenter.startView(this);
        presenter.init();
    }

    @Override
    protected void onDestroyView() {
        presenter.detachView();
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }


    @Override
    public <T> void setData(T data) {
        toast(((Contact)data).getName());
    }

    @Override
    public <T> void transactionActivity(Class<?> activity, T object) {
        super.transactionActivity(activity,true,object);
    }

    @Override
    public void transactionFragmentDialog(DialogFragment fragment) {
        super.transactionFragmentDialog(fragment);
    }

    @Override
    public void transactionActivity(Class<?> activity) {
        super.transactionActivity(activity,true);
    }

    @Override
    public void transitionFragment(Fragment fragment) {
        super.transactionFragmentWithBackStack(fragment);
    }

    @Override
    public void transitionFragmentDialog(DialogFragment fragment) {
        super.transactionFragmentDialog(fragment);
    }

}
