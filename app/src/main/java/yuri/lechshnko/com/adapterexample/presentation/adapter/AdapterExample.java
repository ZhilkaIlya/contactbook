package yuri.lechshnko.com.adapterexample.presentation.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import yuri.lechshnko.com.adapterexample.R;
import yuri.lechshnko.com.adapterexample.data.model.Contact;
import yuri.lechshnko.com.adapterexample.presentation.fragment.main.MainFragmentContract;

public class AdapterExample extends RecyclerView.Adapter<ViewHolderExample> {
    private List<Contact> list;
    private MainFragmentContract.Presenter presenter;

    public AdapterExample(List<Contact>list, MainFragmentContract.Presenter presenter) {
        this.list = list;
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public ViewHolderExample onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolderExample(LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false), parent.getContext(),presenter);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderExample holder, int position) {
            holder.bind(list.get(position),position);
    }

    public void addItem(Contact contact){
        list.add(contact);
        notifyItemInserted(getItemCount());
    }

    public void deleteItem(int id){

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

}
