package yuri.lechshnko.com.adapterexample.app;

import android.app.Application;

import timber.log.Timber;
import yuri.lechshnko.com.adapterexample.data.Repository;
import yuri.lechshnko.com.adapterexample.presentation.Route;


public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.plant(new Timber.DebugTree());
        Route.getInstance();
        Repository.getInstance();
    }
}
