package yuri.lechshnko.com.adapterexample.presentation.fragment.dialog;

import android.os.Bundle;
import yuri.lechshnko.com.adapterexample.R;
import yuri.lechshnko.com.adapterexample.data.model.Contact;
import yuri.lechshnko.com.adapterexample.databinding.FragmentDialogUpdateBinding;
import yuri.lechshnko.com.adapterexample.presentation.base.BaseDialogFragment;

public class DialogUpdateFragment extends BaseDialogFragment<FragmentDialogUpdateBinding> implements DialogUpdateContract.View{
    private static final String TAG_CONTACT = "contact";
    private DialogUpdateContract.Presenter presenter;

    public DialogUpdateFragment() {

    }

    public static DialogUpdateFragment newInstance(Contact contact) {
        DialogUpdateFragment fragment = new DialogUpdateFragment();
        Bundle args = new Bundle();
        args.putParcelable(TAG_CONTACT,contact);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void initDialogView() {
        if (getArguments() != null){
            presenter = new DialogUpdatePresenter();
            presenter.startView(this);
            getBinding().setEvent(presenter);
            Contact contact = getArguments().getParcelable(TAG_CONTACT);
            if (contact != null){
                getBinding().edName.setText(contact.getName());
                getBinding().edNumber.setText(contact.getNumber());
                presenter.init(contact);
            }else{
               stepError(new Throwable());
            }
        }else{
            stepError(new Throwable());
        }
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_dialog_update;
    }


    @Override
    public void onDetach() {
        presenter.detachView();
        presenter = null;
        super.onDetach();
    }

    @Override
    public String getName() {
        return String.valueOf(getBinding().edName.getText());
    }

    @Override
    public String getNumber() {
        return String.valueOf(getBinding().edNumber.getText());
    }

    @Override
    public void nextStep(Contact contact){
       super.nextStep(contact);
       dismiss();
    }

    @Override
    public void stepError(Throwable exception){
       super.stepError(new Throwable());
       dismiss();
    }
}
