package yuri.lechshnko.com.adapterexample.presentation.adapter;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import yuri.lechshnko.com.adapterexample.data.model.Contact;
import yuri.lechshnko.com.adapterexample.databinding.ItemBinding;
import yuri.lechshnko.com.adapterexample.presentation.fragment.main.MainFragmentContract;

public class ViewHolderExample extends RecyclerView.ViewHolder {
    private MainFragmentContract.Presenter presenter;
    private ItemBinding binding;
    private Context context;
    private int positionVisable = -1;

    public ViewHolderExample(@NonNull View itemView, Context context, MainFragmentContract.Presenter presenter) {
        super(itemView);
        this.presenter = presenter;
        this.context = context;
        binding = DataBindingUtil.bind(itemView);
        if (binding != null && presenter != null){
            binding.setEvent(presenter);
        }
    }

    public void bind(Contact item, int pos){
        if (item != null){
            binding.tvName.setText(item.getName());
            binding.tvNumber.setText(item.getNumber());
            binding.cvItem.setOnLongClickListener(v -> {
                positionVisable = pos;
                binding.ivUpdate.setVisibility(View.VISIBLE);
                binding.ivDelete.setVisibility(View.VISIBLE);
                binding.ivUpdate.setOnClickListener(view ->{
                    presenter.eventUpdateItem(item);
                });
                binding.ivDelete.setOnClickListener(view ->{
                    presenter.eventDeleteItem(pos,binding.tvName.getText().toString(),binding.tvNumber.getText().toString());
                });
                return true;
            });
          binding.cvItem.setOnClickListener(v -> {
              binding.ivUpdate.setVisibility(View.GONE);
              binding.ivDelete.setVisibility(View.GONE);
          });
        }
    }
}
