package yuri.lechshnko.com.adapterexample.domain;

import java.util.List;

import io.reactivex.Single;
import yuri.lechshnko.com.adapterexample.data.Repository;
import yuri.lechshnko.com.adapterexample.data.model.Contact;
import yuri.lechshnko.com.adapterexample.domain.base.BaseInteractor;

public class InteractorMain extends BaseInteractor implements InteractorMainContract {


    public InteractorMain() {
    }

    @Override
    public Single<Long> create(String name, String number) {
        return Repository.getInstance().create(name,number)
                .compose(applySingleSchedulers());
    }

    @Override
    public Single<List<Contact>> read() {
        return Repository.getInstance().read()
                .compose(applySingleSchedulers());
    }

//    @Override
//    public Single<Contact> update(Contact contact) {
//        return Repository.getInstance().update(contact)
//                .compose(applySingleSchedulers());
//    }


        @Override
    public Single<Contact> update(Contact contact) {
        return Repository.getInstance().update(contact)
                .compose(applySingleSchedulers());
    }


//
//    @Override
//    public Single<Long> delete(long id) {
//        return Repository.getInstance().delete(id)
//                .compose(applySingleSchedulers());
//    }

    @Override
    public Single<Long> delete(Contact contact) {
        return Repository.getInstance().delete(contact)
                .compose(applySingleSchedulers());
    }


}
