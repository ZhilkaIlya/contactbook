package yuri.lechshnko.com.adapterexample.data.mock;

import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.functions.Function;
import yuri.lechshnko.com.adapterexample.data.model.Contact;

public class Mock implements IMock {
    private List<Contact> list;


    public Mock() {
        init();
    }

    private void init() {
        list = new ArrayList<>();
        int id = 1;

    }


    public void insertList(List<Contact> list) {
        //TODO not used
    }

    @RequiresApi(api = Build.VERSION_CODES.N)

    public Contact query(long id) {
        Contact contact = null;

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getId() == id) {
                contact = list.get(i);
            }
        }

        return contact != null ? contact : new Contact();
    }


    public List<Contact> queryList() {
        return list;
    }

    @Override
    public Single<Long> create(String name, String number) {
        return Single.just(new Contact(name, number))
                .flatMap((Function<Contact, SingleSource<Contact>>) contact -> {
                    if (list != null && list.size() != 0) {
                        long id = list.get(list.size() - 1).getId();
                        contact.setId(id);
                        return Single.just(contact);
                    } else if (list.size() == 0) {
                        contact.setId(1);
                        return Single.just(contact);
                    }else {
                        list = new ArrayList<>();
                        contact.setId(1);
                        return Single.just(contact);
                    }
                })
                .flatMap(contact -> {
                    list.add(contact);

                    return Single.just(contact.getId());
                });
    }


    private Single<Contact> generateId(Contact contact){
        if (list != null) {
            long id = list.get(list.size()-1).getId();
            contact.setId(id);
            return Single.just(contact);
        }else {
            list = new ArrayList<>();
            contact.setId(1);
            return Single.just(contact);
        }
    }
//
//    private Single<Long> insert(Contact  contact) {
//        list.add(contact);
//        return Single.just(contact.getId());
//    }

    @Override
    public Single<List<Contact>> read() {
        return Single.just(list);
    }

    @Override
    public Single<Contact> update(Contact contact) {
        return Single.defer(()-> {
            Log.e("After", String.valueOf(list.indexOf(contact)));
            return Single.just(contact);
        });
    }


    public long deleteEntity(long id) {
        int mId = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getId() == id){
                mId = i;
            }
        }
        if (mId != 0){
            list.remove(mId);
        }
        return id;
    }


    @Override
    public Single<Long> delete(Contact contact) {
        return Single.defer(()-> {
            long id = contact.getId();
        list.remove(contact);

        return Single.just(id);

    });

        }

    public void clearAll() {
        list.clear();
    }
}
