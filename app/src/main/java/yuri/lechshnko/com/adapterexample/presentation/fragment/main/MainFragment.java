package yuri.lechshnko.com.adapterexample.presentation.fragment.main;


import androidx.recyclerview.widget.LinearLayoutManager;
import java.util.List;

import yuri.lechshnko.com.adapterexample.R;
import yuri.lechshnko.com.adapterexample.data.model.Contact;
import yuri.lechshnko.com.adapterexample.databinding.FragmentMainBinding;
import yuri.lechshnko.com.adapterexample.presentation.adapter.AdapterExample;
import yuri.lechshnko.com.adapterexample.presentation.base.BaseFragment;
import yuri.lechshnko.com.adapterexample.presentation.base.BasePresenter;


public class MainFragment extends BaseFragment<FragmentMainBinding> implements MainFragmentContract.View {
    private AdapterExample adapter;
    private MainFragmentContract.Presenter presenter;

    public MainFragment() {

    }

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_main;
    }

    @Override
    protected void initFragmentView() {
        presenter = new MainFragPresenter();
        getBinding().setEvent(presenter);
        presenter.init();
    }

    @Override
    protected void startFragment() {
        presenter.startView(this);
    }

    @Override
    protected void stopFragment() {

    }

    @Override
    protected void destroyFragment() {
        if (presenter != null){
            presenter = null;
        }
    }

    @Override
    protected void pauseFragment() {

    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }


    @Override
    public void initAdapter(List<Contact> list) {
        getBinding().rvExample.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        adapter =new AdapterExample(list,presenter);
        getBinding().rvExample.setAdapter(adapter);
    }

    @Override
    public void addItem(Contact contact) {
        adapter.addItem(contact);
    }

    @Override
    public void deleteItem(int id) {
        adapter.deleteItem(id);
    }


    @Override
    public Contact eventAddContact() {
        return new Contact(String.valueOf(getBinding().edName.getText()),String.valueOf(getBinding().edNumber.getText()));
    }

    @Override
    public void message(String val) {
        toastLong(val);
    }

    @Override
    public void setName(String string) {
        getBinding().edName.setText(string);
    }

    @Override
    public String getName() {
        return getBinding().edName.getText().toString();
    }

    @Override
    public void setNumber(String string) {
        getBinding().edNumber.setText(string);
    }

    @Override
    public String getNumber() {
        return getBinding().edNumber.getText().toString();
    }


}
