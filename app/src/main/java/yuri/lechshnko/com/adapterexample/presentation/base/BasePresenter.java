package yuri.lechshnko.com.adapterexample.presentation.base;

public interface BasePresenter<V> {

    void startView(V view);

    void detachView();
}
