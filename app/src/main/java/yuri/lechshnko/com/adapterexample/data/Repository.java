package yuri.lechshnko.com.adapterexample.data;

import java.util.List;

import io.reactivex.Single;
import yuri.lechshnko.com.adapterexample.data.mock.IMock;
import yuri.lechshnko.com.adapterexample.data.mock.Mock;
import yuri.lechshnko.com.adapterexample.data.model.Contact;

public class Repository implements RepositoryContract{
    private static RepositoryContract instance;
    private IMock iMock;

    private Repository(){
        iMock = new Mock();
    }


    public static synchronized RepositoryContract getInstance(){
        if (instance == null){
            instance = new Repository();
        }
        return instance;
    }

    @Override
    public Single<Long> create(String name, String number) {
        return iMock.create(name,number);
    }

    @Override
    public Single<List<Contact>> read() {
        return iMock.read();
    }

    @Override
    public Single<Contact> update(Contact contact) {
        return iMock.update(contact);
    }

//    @Override
//    public Single<Contact> update(Contact contact) {
//        return iMock.update(contact);
//    }

//    @Override
//    public Single<Long> delete(long id) {
//        return iMock.deleteEntity(id);
//    }

    @Override
    public Single<Long> delete(Contact contact) {
        return iMock.delete(contact);
    }
}
