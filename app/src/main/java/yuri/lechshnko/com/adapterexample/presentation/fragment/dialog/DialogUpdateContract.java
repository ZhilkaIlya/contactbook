package yuri.lechshnko.com.adapterexample.presentation.fragment.dialog;

import yuri.lechshnko.com.adapterexample.data.model.Contact;
import yuri.lechshnko.com.adapterexample.presentation.base.BasePresenter;

public interface DialogUpdateContract {
    interface View {
        String getName();

        String getNumber();

        void nextStep(Contact contact);

        void stepError(Throwable exception);
    }

    interface Presenter extends BasePresenter<View> {
        void init(Contact contact);

        void onClickOk();

        void onClickCancel();
    }
}
