package yuri.lechshnko.com.adapterexample.presentation.fragment.main;

import android.util.Log;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import timber.log.Timber;
import yuri.lechshnko.com.adapterexample.data.model.Contact;
import yuri.lechshnko.com.adapterexample.domain.InteractorMain;
import yuri.lechshnko.com.adapterexample.domain.InteractorMainContract;
import yuri.lechshnko.com.adapterexample.presentation.Route;

public class MainFragPresenter  implements MainFragmentContract.Presenter {
    private InteractorMainContract interactor;
    private MainFragmentContract.View view;



    @Override
    public void init() {
        interactor = new InteractorMain();
        interactor.read()
                .subscribe(new DisposableSingleObserver<List<Contact>>() {
                    @Override
                    public void onSuccess(List<Contact> contacts) {
                        view.initAdapter(contacts);
                        dispose();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e("read error %s",e.getMessage());
                        view.message(e.getMessage());
                    }
                });
    }

    @Override
    public void eventUpdateItem(Contact item) {
        String redult = ""+ item.getId()+item.getNumber()+item.getName();
        Log.e("Before",redult);
        Route.getInstance().startDialog(item)
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<Contact>() {
                    @Override
                    public void onSuccess(Contact contact) {
                        interactor.update(new Contact(contact.getId(),contact.getName(),contact.getNumber()))
                                .subscribe(new DisposableSingleObserver<Contact>() {

                            @Override
                            public void onSuccess(Contact contact) {
                                String redult = ""+ contact.getId()+contact.getNumber()+contact.getName();
                                Log.e("Before",redult);

                            }

                            @Override
                            public void onError(Throwable e) {

                            }
                        });
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.message("Cancel");
                    }
                });
    }

    @Override
    public void eventDeleteItem(int id,String name, String number) {
        Contact contact = new Contact(id,name,number);
        interactor.delete(contact).subscribe(
                new DisposableSingleObserver<Long>() {
                    @Override
                    public void onSuccess(Long aLong) {
                    view.deleteItem(id);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.message("Ding dong , you are wrong");
                    }
                });
    }

    @Override
    public void eventAddItem() {
        String name = view.getName();
        String number = view.getNumber();

        interactor.create(name,number).subscribe(
                new DisposableSingleObserver<Long>() {
                    @Override
                    public void onSuccess(Long aLong) {
                       view.addItem(view.eventAddContact());

                    }

                    @Override
                    public void onError(Throwable e) {
                        view.message("Ding dong , you are wrong");
                    }
                });
    }



    @Override
    public void startView(MainFragmentContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        if (view != null) view = null;
    }
}
