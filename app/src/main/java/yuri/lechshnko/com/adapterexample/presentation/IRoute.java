package yuri.lechshnko.com.adapterexample.presentation;

import io.reactivex.Single;
import yuri.lechshnko.com.adapterexample.data.model.Contact;
import yuri.lechshnko.com.adapterexample.presentation.base.BaseActivityContract;

public interface IRoute {
    void startView(BaseActivityContract view);

    void stopView();

    <T>void transaction(String cmd,T data);

    <T>void setDataDialog(T data);

    Single<Contact> startDialog(Contact contact);
}
