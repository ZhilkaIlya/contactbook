package yuri.lechshnko.com.adapterexample.presentation.base;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.DialogFragment;
import io.reactivex.SingleEmitter;
import yuri.lechshnko.com.adapterexample.data.model.Contact;

public abstract class BaseDialogFragment<Binding extends ViewDataBinding> extends DialogFragment {
    private Binding binding;
    private SingleEmitter<Contact> emitter = null;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false);
        initDialogView();
        return binding.getRoot();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    protected abstract void initDialogView();

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @LayoutRes
    protected abstract int getLayoutRes();

    protected Binding getBinding() {
        return binding;
    }

    protected void toast(String message){
        Toast.makeText(getActivity(),message, Toast.LENGTH_SHORT).show();
    }

    public void nextStep(Contact contact){
        if(emitter != null){
            emitter.onSuccess(contact);
        }
    }

    public void stepError(Throwable exception){
        if(emitter != null){
            emitter.onError(exception);
        }
    }

    public void setEmitter(SingleEmitter<Contact> emitter) {
        this.emitter = emitter;
    }
}
